﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guitar : MonoBehaviour {

    public Transform firepoint;

    public GameObject bullPrefab;

    public Transform meleepoint;

    public GameObject meleePrefab;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Shoot();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Melee();
           
        }
	}
    void Shoot()
    {
       GameObject bullet = (GameObject)Instantiate(bullPrefab, firepoint.position, firepoint.rotation);
        Destroy(bullet, 0.5f);
    }
    void Melee()
    {
        Debug.Log("Swoosh");
        GameObject melee = (GameObject)Instantiate(meleePrefab, meleepoint.position, meleepoint.rotation);
        Destroy(melee, 0.1f);
    }
}
