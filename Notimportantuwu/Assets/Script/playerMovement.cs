﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{

    public float speed;
    public float jump = 5f;
    public Rigidbody2D rb2d;
    public bool IsWalking = true;
    public bool IsInLadder = false;

    public Transform teleportTransform;

       // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        enemykill enemykill = GetComponent<enemykill>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        IsWalking = true;

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Ladder")
        {
            Debug.Log("LAdder");
            
                IsInLadder = true;
                Debug.Log("OnLAdder");
        }

        if (collision.gameObject.tag == "TeleportTrigger")
        {
            this.transform.position = teleportTransform.position;
        }
     
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        
        IsInLadder = false;
        rb2d.gravityScale = 1;
        Debug.Log("Exit Ladder");
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            this.transform.position = teleportTransform.position;
        }

        float move = Input.GetAxis("Horizontal");

        rb2d.velocity = new Vector2(speed * move, rb2d.velocity.y);

        if (IsWalking == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
        }
        
        if (IsInLadder == true)
        {
           
            Climb();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
                rb2d.gravityScale = 1;
                IsInLadder = false;
            }
        }

    }
    void Jump()
    {
        rb2d.velocity = new Vector2(0, jump);
        IsWalking = false;
    }
    void Climb()
    {
        
        
            float laddermovement = Input.GetAxis("Vertical");
            rb2d.gravityScale = 0;
            rb2d.velocity = new Vector2(rb2d.velocity.x, laddermovement * speed);
        
        
    }
}
