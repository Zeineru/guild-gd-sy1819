﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class enemykill : MonoBehaviour {

    public bool IsKillable = true;
    public GameObject player;
    public float speed;
    public float health = 100f;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(player);
            SceneManager.LoadScene("SampleScene");
        }
       

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        speed = -speed;
    }

    public void Update()
    {
        transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
    }
    public void Damage (float damage)
    {
        health = health - damage;
        if(health <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        Destroy(gameObject);
    }
}
