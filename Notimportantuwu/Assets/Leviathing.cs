﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leviathing : MonoBehaviour {
    public GameObject pointa;
    public GameObject pointb;
    public float speed;
    public GameObject platform;
    public Rigidbody2D rb2d;
   

    public void Start()
    {
       rb2d = rb2d.GetComponent<Rigidbody2D>();
       
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")

        {
         
            collision.collider.transform.SetParent(transform);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        speed = -speed;

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(null);
    }

    // Update is called once per frame
    void Update () { 
        {
            transform.position += new Vector3(0, speed * Time.deltaTime, 0); // for positioning
        }
        
    }
}
