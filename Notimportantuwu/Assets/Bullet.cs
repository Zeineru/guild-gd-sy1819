﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed = 20f;
    public Rigidbody2D rb2d;
    public float damage = 10f;
	// Use this for initialization

	void Start () {
        rb2d.velocity = transform.right * speed;
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
       enemykill enemy = collision.GetComponent<enemykill>();
        if(enemy != null)
        {
            enemy.Damage(damage);
            Destroy(gameObject);
        }
    }
}
