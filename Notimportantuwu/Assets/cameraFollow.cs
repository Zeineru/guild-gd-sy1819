﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {

    public Transform player;
    public float smoothness = 0.125f;
    public Vector3 offset; 

     void FixedUpdate()
    {
        Vector3 desiredPosition = player.position + offset;
        Vector3 smmothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothness);
        transform.position = smmothedPosition;

        transform.LookAt(player);
    }
}
